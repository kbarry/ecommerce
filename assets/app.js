// assets/app.js
import { createApp } from 'vue';
import App from './js/app.vue';
import { createVuetify } from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

const vuetify = createVuetify();
const app = createApp(App);

// Récupérer l'URL de l'API à partir du conteneur div
const apiUrl = document.getElementById('vueApp').getAttribute('data-api-url');

app.provide('apiUrl', apiUrl);  // Fournir l'URL de l'API à tous les composants
app.use(vuetify);
app.mount('#vueApp');
