import { startStimulusApp } from '@symfony/stimulus-bridge';

// Registers Stimulus controllerss from controllerss.json and in the controllerss/ directory
export const app = startStimulusApp(require.context(
    '@symfony/stimulus-bridge/lazy-controller-loader!./controllerss',
    true,
    /\.[jt]sx?$/
));
// register any custom, 3rd party controllerss here
// app.register('some_controller_name', SomeImportedController);
