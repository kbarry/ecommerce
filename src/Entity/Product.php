<?php
// No comments or whitespace before the opening <?php tag

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Categorie;
use Symfony\Component\Serializer\Attribute\Groups;

// Rest of your class definition follows...


#[ORM\Entity(repositoryClass: ProductRepository::class)]
    class Product
    {
        #[ORM\Id]
        #[ORM\GeneratedValue]
        #[ORM\Column]
        #[Groups(['produit', 'categorie'])]
        private ?int $id = null;

        #[ORM\Column(length: 255, nullable: true)]
        #[Groups(['produit', 'categorie'])]
        private ?string $name = null;

        #[ORM\Column(nullable: true)]
        #[Groups(['produit', 'categorie'])]
        private ?float $prix = null;

        #[ORM\Column(type: Types::TEXT, nullable: true)]
        #[Groups(['produit', 'categorie'])]
        private ?string $description = null;

        #[ORM\Column(length: 255, nullable: true)]
        #[Groups(['produit', 'categorie'])]
        private ?string $image = null;
        #[Groups(['produit'])]
        #[ORM\ManyToOne(inversedBy: 'produits')]

        private ?Categorie $categories = null;

        /**
         * @var Collection<int, Panier>
         */
        #[ORM\ManyToMany(targetEntity: Panier::class, mappedBy: 'produit')]
        private Collection $paniers;

        /**
         * @var Collection<int, Commandes>
         */
        #[ORM\ManyToMany(targetEntity: Commandes::class, mappedBy: 'produit')]
        private Collection $commandes;

        /**
         * @var Collection<int, PanierProducts>
         */
        #[ORM\OneToMany(targetEntity: PanierProducts::class, mappedBy: 'product')]
        private Collection $panierProducts;

        /**
         * @var Collection<int, ProductCommandes>
         */
        #[ORM\OneToMany(targetEntity: ProductCommandes::class, mappedBy: 'product')]
        private Collection $productCommandes;





        public function __construct()
        {
            $this->paniers = new ArrayCollection();
            $this->commandes = new ArrayCollection();
            $this->productCommandes = new ArrayCollection();

        }

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getName(): ?string
        {
            return $this->name;
        }

        public function setName(?string $name): static
        {
            $this->name = $name;

            return $this;
        }

        public function getPrix(): ?float
        {
            return $this->prix;
        }

        public function setPrix(?float $prix): static
        {
            $this->prix = $prix;

            return $this;
        }

        public function getDescription(): ?string
        {
            return $this->description;
        }

        public function setDescription(?string $description): static
        {
            $this->description = $description;

            return $this;
        }

        public function getImage(): ?string
        {
            return $this->image;
        }

        public function setImage(?string $image): static
        {
            $this->image = $image;

            return $this;
        }

        public function getCategories(): ?Categorie
        {
            return $this->categories;
        }

        public function setCategories(?Categorie $categories): static
        {
            $this->categories = $categories;

            return $this;
        }

        /**
         * @return Collection<int, Panier>
         */
        public function getPaniers(): Collection
        {
            return $this->paniers;
        }

        public function addPanier(Panier $panier): static
        {
            if (!$this->paniers->contains($panier)) {
                $this->paniers->add($panier);
                $panier->addProduit($this);
            }

            return $this;
        }

        public function removePanier(Panier $panier): static
        {
            if ($this->paniers->removeElement($panier)) {
                $panier->removeProduit($this);
            }

            return $this;
        }

        /**
         * @return Collection<int, Commandes>
         */
        public function getCommandes(): Collection
        {
            return $this->commandes;
        }

        public function addCommande(Commandes $commande): static
        {
            if (!$this->commandes->contains($commande)) {
                $this->commandes->add($commande);
                $commande->addProduit($this);
            }

            return $this;
        }

        public function removeCommande(Commandes $commande): static
        {
            if ($this->commandes->removeElement($commande)) {
                $commande->removeProduit($this);
            }

            return $this;
        }

        /**
         * @return Collection<int, PanierProducts>
         */
        public function getPanierProducts(): Collection
        {
            return $this->panierProducts;
        }

        public function addPanierProduct(PanierProducts $panierProduct): static
        {
            if (!$this->panierProducts->contains($panierProduct)) {
                $this->panierProducts->add($panierProduct);
                $panierProduct->setProduct($this);
            }

            return $this;
        }

        public function removePanierProduct(PanierProducts $panierProduct): static
        {
            if ($this->panierProducts->removeElement($panierProduct)) {
                // set the owning side to null (unless already changed)
                if ($panierProduct->getProduct() === $this) {
                    $panierProduct->setProduct(null);
                }
            }

            return $this;
        }

        /**
         * @return Collection<int, ProductCommandes>
         */
        public function getProductCommandes(): Collection
        {
            return $this->productCommandes;
        }

        public function addProductCommande(ProductCommandes $productCommande): static
        {
            if (!$this->productCommandes->contains($productCommande)) {
                $this->productCommandes->add($productCommande);
                $productCommande->setProduct($this);
            }

            return $this;
        }

        public function removeProductCommande(ProductCommandes $productCommande): static
        {
            if ($this->productCommandes->removeElement($productCommande)) {
                // set the owning side to null (unless already changed)
                if ($productCommande->getProduct() === $this) {
                    $productCommande->setProduct(null);
                }
            }

            return $this;
        }



    }
