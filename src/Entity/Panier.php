<?php

namespace App\Entity;

use App\Repository\PanierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PanierRepository::class)]
class Panier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'panier', cascade: ['persist', 'remove'])]
    private ?User $user = null;

    /**
     * @var Collection<int, Product>
     */
    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'paniers')]
    private Collection $produit;

    /**
     * @var Collection<int, PanierProducts>
     */
    #[ORM\OneToMany(targetEntity: PanierProducts::class, mappedBy: 'panier')]
    private Collection $productPanier;



    public function __construct()
    {
        $this->produit = new ArrayCollection();
        $this->panierProducts = new ArrayCollection();
        $this->productPanier = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProduit(): Collection
    {
        return $this->produit;
    }

    public function addProduit(Product $produit): static
    {
        if (!$this->produit->contains($produit)) {
            $this->produit->add($produit);
        }

        return $this;
    }

    public function removeProduit(Product $produit): static
    {
        $this->produit->removeElement($produit);

        return $this;
    }

    /**
     * @return Collection<int, PanierProducts>
     */
    public function getProductPanier(): Collection
    {
        return $this->productPanier;
    }

    public function addProductPanier(PanierProducts $productPanier): static
    {
        if (!$this->productPanier->contains($productPanier)) {
            $this->productPanier->add($productPanier);
            $productPanier->setPanier($this);
        }

        return $this;
    }

    public function removeProductPanier(PanierProducts $productPanier): static
    {
        if ($this->productPanier->removeElement($productPanier)) {
            // set the owning side to null (unless already changed)
            if ($productPanier->getPanier() === $this) {
                $productPanier->setPanier(null);
            }
        }

        return $this;
    }


}
