<?php

namespace App\Entity;

use App\Repository\ProductCommandesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductCommandesRepository::class)]
class ProductCommandes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'productCommandes')]
    private ?Commandes $commandes = null;

    #[ORM\ManyToOne(inversedBy: 'productCommandes')]
    private ?Product $product = null;

    #[ORM\ManyToOne(inversedBy: 'productCommandes')]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommandes(): ?Commandes
    {
        return $this->commandes;
    }

    public function setCommandes(?Commandes $commandes): static
    {
        $this->commandes = $commandes;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): static
    {
        $this->product = $product;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
