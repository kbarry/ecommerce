<?php

namespace App\Entity;

use App\Repository\CommandesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommandesRepository::class)]
class Commandes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    /**
     * @var Collection<int, Product>
     */
    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'commandes')]
    private Collection $produit;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $datecreate = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'commandes')]
    private Collection $user;

    /**
     * @var Collection<int, ProductCommandes>
     */
    #[ORM\OneToMany(targetEntity: ProductCommandes::class, mappedBy: 'commandes')]
    private Collection $productCommandes;


    public function __construct()
    {
        $this->produit = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->productCommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProduit(): Collection
    {
        return $this->produit;
    }

    public function addProduit(Product $produit): static
    {
        if (!$this->produit->contains($produit)) {
            $this->produit->add($produit);
        }

        return $this;
    }

    public function removeProduit(Product $produit): static
    {
        $this->produit->removeElement($produit);

        return $this;
    }

    public function getDatecreate(): ?\DateTimeInterface
    {
        return $this->datecreate;
    }

    public function setDatecreate(?\DateTimeInterface $datecreate): static
    {
        $this->datecreate = $datecreate;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): static
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
            $user->setCommandes($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCommandes() === $this) {
                $user->setCommandes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductCommandes>
     */
    public function getProductCommandes(): Collection
    {
        return $this->productCommandes;
    }

    public function addProductCommande(ProductCommandes $productCommande): static
    {
        if (!$this->productCommandes->contains($productCommande)) {
            $this->productCommandes->add($productCommande);
            $productCommande->setCommandes($this);
        }

        return $this;
    }

    public function removeProductCommande(ProductCommandes $productCommande): static
    {
        if ($this->productCommandes->removeElement($productCommande)) {
            // set the owning side to null (unless already changed)
            if ($productCommande->getCommandes() === $this) {
                $productCommande->setCommandes(null);
            }
        }

        return $this;
    }


}
