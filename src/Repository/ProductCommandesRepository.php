<?php

namespace App\Repository;

use App\Entity\ProductCommandes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Entity\Commandes;
/**
 * @extends ServiceEntityRepository<ProductCommandes>
 *
 * @method ProductCommandes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCommandes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCommandes[]    findAll()
 * @method ProductCommandes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCommandesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCommandes::class);
    }

    //    /**
    //     * @return ProductCommandes[] Returns an array of ProductCommandes objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ProductCommandes
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
    public function findByUser(User $user)
    {
        $qb = $this->createQueryBuilder('pc')
            ->select('c.datecreate as date')
            ->addSelect('p.image, p.name,p.prix')
            ->leftJoin('pc.product', 'p')
            ->leftJoin('pc.commandes', 'c')
            ->leftJoin('pc.user', 'u')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->getQuery();

        return $qb->getResult();
    }

}
