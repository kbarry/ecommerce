<?php

namespace App\Repository;

use App\Entity\Panier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

/**
 * @extends ServiceEntityRepository<Panier>
 *
 * @method Panier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Panier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Panier[]    findAll()
 * @method Panier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PanierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Panier::class);
    }

    //    /**
    //     * @return Panier[] Returns an array of Panier objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Panier
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
    public function findWithProduits($id){
        $qb=$this->createQueryBuilder('panier')
        ->leftjoin('panier.produit','produit')
            ->andwhere('produit.id=:id')
            ->setParameter('id',$id)
            ->getQuery();

        return $qb->getResult();


    }
    public function findProduits($id){

             $qb = $this->createQueryBuilder('pa')
                 ->select('pp.quantity as quatity')
                 ->addSelect('p.name, p.image, p.prix')
                 ->leftJoin('pa.produit', 'p')
                 ->leftjoin('pa.productPanier','pp')
                 ->andwhere('p.id = :id')
                 ->setParameter('id', $id)
                 ->getQuery();


         return $qb->getResult();
    }
    public function findAllProduits(){
        $qb=$this->createQueryBuilder('pa')
            ->select('pp.quantity as quatity')
            ->addSelect('p.name, p.image, p.prix')
            ->leftJoin('pa.produit', 'p')
            ->leftjoin('pa.productPanier','pp')

            ->getQuery();


        return $qb->getResult();

    }



}
