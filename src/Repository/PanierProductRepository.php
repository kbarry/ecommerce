<?php

namespace App\Repository;

use App\Entity\PanierProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Panier;
use App\Entity\Product;
use App\Entity\User;

/**
 * @extends ServiceEntityRepository<PanierProducts>
 *
 * @method PanierProducts|null find($id, $lockMode = null, $lockVersion = null)
 * @method PanierProducts|null findOneBy(array $criteria, array $orderBy = null)
 * @method PanierProducts[]    findAll()
 * @method PanierProducts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PanierProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PanierProducts::class);
    }

    //    /**
    //     * @return PanierProducts[] Returns an array of PanierProducts objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PanierProducts
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }

    public function findWithPanierProduit(Panier $panier, Product $product)
    {
        $qb = $this->createQueryBuilder('panierproduct')
            ->leftJoin('panierproduct.product', 'produits')
            ->leftJoin('panierproduct.panier', 'panier')
            ->where('produits = :product')
            ->andWhere('panier = :panier')
            ->setParameter('product', $product)
            ->setParameter('panier', $panier)
            ->getQuery();

        return $qb->getOneOrNullResult();
    }
    public function getCartItemsWithTotalPrice(User $user): array
    {
        $queryBuilder = $this->createQueryBuilder('pp')
            ->select('pp.id')
            ->addselect('p.name, p.image, p.prix, pp.quantity as quantity')
            ->addSelect('(p.prix * pp.quantity) as totalPrice') // Ajout du calcul du total
            ->leftJoin('pp.product', 'p')
            ->leftJoin('pp.panier', 'pa')
            ->leftJoin('pa.user', 'u')
            ->andWhere('u = :user')
            ->setParameter('user', $user);

        return $queryBuilder
            ->getQuery()
            ->getResult();
    }



}
