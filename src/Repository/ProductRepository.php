<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)

    {

        parent::__construct($registry, Product::class);
    }

//    /**
//     * @return Product[] Returns an array of Product objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Product
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function findRand($limit)
    {
        // Récupérer le nombre total d'éléments
        $total = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();

        // Générer un nombre aléatoire pour l'offset
        $offset = mt_rand(0, max(0, $total - $limit));

        // Récupérer un sous-ensemble aléatoire
        return $this->createQueryBuilder('p')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
    public function findWithCategories($id){
        $qb = $this->createQueryBuilder('p')
            ->join('p.categories', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->orderBy('c.name', 'ASC');

        return $qb->getQuery()->getResult();

    }
    public function findDetails($id) {
        $qb = $this->createQueryBuilder('p')
            ->select('p.id', 'p.name', 'p.prix', 'p.image', 'c.name as nomCat')
            ->leftJoin('p.categories', 'c')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $qb->getResult();
    }


}
