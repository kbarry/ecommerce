<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductRepository;
use App\Repository\CategorieRepository;

class  DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(ProductRepository $src): Response
    {
      $limit=15;
        $produit=$src->findRand($limit);
        return $this->render('default/index.html.twig', [
           'produit'=>$produit
        ]);
    }
    #[Route('/categorie.html',name:'app_categorie')]
    public function categories(CategorieRepository $categorieRepository): Response
    {
        $categories = $categorieRepository->findRandCategorie();

        return $this->render('categorie.html.twig', [
            'categories' => $categories,
        ]);
    }
    #[Route('/produitCategorie.html/{id}',name:'app_produit_categorie')]
public function produitCategorie(ProductRepository $src, $id){
        $produits=$src->findWithCategories($id);
        return $this->render('/produitCategorie.html.twig',['produits'=>$produits]);
    }
}
