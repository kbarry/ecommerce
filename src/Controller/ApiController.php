<?php

namespace App\Controller;

use App\Repository\CategorieRepository;
use App\Repository\PanierProductRepository;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductRepository;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\UserRepository;


#[Route('/api')]
class ApiController extends AbstractController
{
    #[Route('/produits', name: 'api_produits')]
    public function apiProduits(ProductRepository $produitRepository): Response
    {
        // on récupère l'entité
        $produits = $produitRepository->findAll();
        // need composer require symfony/serializer
        // on transforme en json
        return $this->json($produits, Response::HTTP_OK, [], [
            // voir la définition des groupes dans l'entité
            'groups' => ['produit']
        ]);
    }

    #[Route('/categories', name: 'api_categories' )]
    public function apiCategories(CategorieRepository $categorieRepository): Response
    {
        // on récupère l'entité
        $categories = $categorieRepository->findAll();
        // need composer require symfony/serializer
        // on transforme en json
        return $this->json($categories,Response::HTTP_OK,[],
            [
                // voir la définition des groupes dans l'entité
                'groups' => ['categorie']
            ]
        );
    }
    #[Route('/produitCategorie.html/{id}',name:'api_produit_categorie')]
    public function produitCategorie(ProductRepository $src, $id){
        $produitsCategorie=$src->findWithCategories($id);
        return $this->json($produitsCategorie,Response::HTTP_OK,[],
            [
                // voir la définition des groupes dans l'entité
                'groups' => ['produit']
            ]
        );

    }
    #[Route ('/cart',name:'appi_cart')]
    public function panier(PanierProductRepository $em){

        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();
        $cartItems = $em->getCartItemsWithTotalPrice($user);

        $total = 0.0;

        foreach ($cartItems as $item) {

            $total += $item['totalPrice'];
        }
        return $this->json($cartItems,Response::HTTP_OK,[],
            [
                // voir la définition des groupes dans l'entité
                'groups' => ['produit']
            ]
        );
    }

    #[Route('/users', name: 'api_login' )]
    public function apiUser(UserRepository $user): Response
    {

        $users = $user->findAll();

        return $this->json($users,Response::HTTP_OK,[],
            [

                'groups' => ['user']
            ]
        );
    }

}

