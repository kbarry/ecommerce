<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductRepository;

class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit')]
    public function index(ProductRepository $src): Response
    {
        $produits=$src->findAll();
        return $this->render('produit/index.html.twig', [
            'produits' => $produits,
        ]);
    }
    #[Route('/details/{id}',name:'app_detail')]
public function detail($id, ProductRepository $src){
        $product=$src->find($id);
        return $this->render('produit/detail.html.twig',['product'=>$product]);
    }
}
