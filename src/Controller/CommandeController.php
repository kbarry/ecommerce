<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductCommandesRepository;
use App\Entity\User;
use App\Entity\Commandes;
use App\Entity\ProductCommandes;
use APP\Entity\PanierProducts;
use Doctrine\Persistence\ObjectManager;
class CommandeController extends AbstractController
{
    #[Route('/commande', name: 'app_commande')]
    public function index(ProductCommandesRepository $src,EntityManagerInterface $manager): Response
    {
        $user =$this->getUser();

        if(!$user){
            return $this->redirectToRoute('app_login');

        }
        $panier=$user->getPanier();
        if($panier==NULL){
            return $this->redirectToRoute('app_categorie');

        }
        if (!$panier || $panier->getProductPanier()->isEmpty()) {
            $this->addFlash('info', 'Votre panier est vide');
            return $this->redirectToRoute('app_categorie');
        }
        $productPanier=$panier->getProductPanier();

        //var_dump($productPanier);
        $commande=new Commandes();
        $dateDujour=new \DateTime();
        $commande->setDatecreate($dateDujour);
        $dateString = $dateDujour->format('Ymd_His');
        $commande->setName('CMD_'.$dateString);
        $commande->addUser($user);
        $user->setCommandes($commande);
        $manager->persist($commande);
        foreach($productPanier as $panier){

            $productCommandes=new ProductCommandes();
            $productCommandes->setCommandes($commande);
            $productCommandes->setProduct($panier->getProduct());
            $productCommandes->setUser($user);
            $commande->addProduit($panier->getProduct());
            $panier->getProduct()->addCommande($commande);
            $manager->persist($productCommandes);
            $produit=$panier->getProduct();
            $paniers=$panier->getPanier();
            $produit->removePanier($paniers);
            $manager->remove($panier);
        }
        $manager->flush();
        $commandes=$src->findByUser($user);
        return $this->render('commande/index.html.twig', [
            'commandes' => $commandes
        ]);
    }
#[Route('/commandes', name: 'app_commandes')]
public function commande(ProductCommandesRepository $src):response{
    $user =$this->getUser();

    if(!$user){
        return $this->redirectToRoute('app_login');

    }
    $commandes=$src->findByUser($user);
    return $this->render('commande/commande.html.twig', [
        'commandes' => $commandes
    ]);
}
}
