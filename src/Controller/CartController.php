<?php

namespace App\Controller;

use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request; // Import correct pour Request
//use App\Repository\ProductRepository; // Assurez-vous que le chemin est correct
use App\Entity\Panier;
use App\Repository\PanierRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\PanierProducts;
use App\Repository\PanierProductRepository;


class CartController extends AbstractController
{
    #[Route('/cart/{id}', name: 'app_cart')]
    public function addToCart(PanierProductRepository $em, ProductRepository $produitRepository, Request $request, $id, PanierRepository $src, EntityManagerInterface $manager): Response
    {
        $quantite=0;

        $quantity = 1;
        $product = $produitRepository->find($id);


        if ($this->getUser() == NULL) {
            return $this->redirectToRoute('app_login');
        }
        $user = $this->getUser();
        $panier = $user->getPanier();
        if ($panier != NULL) {
           $productPanier=$em->findWithpanierProduit($panier,$product);
           if($productPanier!=NUll){
               $quantite=$productPanier->getQuantity()+$quantity;
               $productPanier->setQuantity($quantite);
               $panier->setUser($user);
           }


            else{
                $product_panier=new PanierProducts();
                    $panier->addProduit($product);
                    $panier->setUser($user);
                    $panier->addProductPanier($product_panier);
                    $product->addPanierProduct($product_panier);
                    $product_panier->setProduct($product);
                    $product_panier->setPanier($panier);
                    $product_panier->setQuantity($quantity);
                    $manager->persist($product_panier);

                }
            }


        else{
            $panier=new Panier();
            $product_panier=new PanierProducts();
            $product_panier->setQuantity($quantity);
            $panier->addProduit($product);
            $panier->addProductPanier($product_panier);
            $product->addPanierProduct($product_panier);
            $panier->setUser($user);
            $product_panier->setProduct($product);
            $product_panier->setPanier($panier);
            $manager->persist($panier);
            $manager->persist($product_panier);
        }
    $manager->flush();
        $cartItems = $em->getCartItemsWithTotalPrice($user);

        $total = 0.0;

        foreach ($cartItems as $item) {

            $total += $item['totalPrice'];
        }
        return $this->render('cart/index.html.twig', [
            'cartItems' => $cartItems, 'total'=>$total,
        ]);
    }
    #[Route ('/Ajouter/{id}', name:'app_cart_ajout')]
public function ajout($id,PanierProductRepository $em,EntityManagerInterface $manager){
        $quantity=1;
        $quantite=0;
        $id1=0;
        $productpanierId=$em->find($id);
        $id1=$productpanierId->getProduct()->getId();

        $quantite=$productpanierId->getQuantity()+$quantity;
        $productpanierId->setQuantity($quantite);
        $manager->persist($productpanierId);
        $manager->flush();
        return $this->redirectToRoute('app_panier');
}
    #[Route('/Reduire/{id}', name: 'app_cart_reduire')]
    public function reduire($id, PanierProductRepository $em,EntityManagerInterface $manager)
    {
        $nombre=0;
       $quantity=1;
        $produitpanier = $em->find($id);


        // Obtenez l'ID du produit
        $productId = $produitpanier->getProduct()->getId();

        // Vérifiez que la quantité n'est pas déjà nulle avant de la réduire
        $nombre = $produitpanier->getQuantity()-$quantity;
        var_dump($nombre);
        if ($nombre > 0) {

            $produitpanier->setQuantity($nombre);
            $manager->persist($produitpanier);
            $manager->flush();
        }


        return $this->redirectToRoute('app_panier');
    }
#[Route ('/panier',name:'app_panier')]
public function panier(PanierProductRepository $em){

    if ($this->getUser() == NULL) {
        return $this->redirectToRoute('app_login');
    }
    $user = $this->getUser();
    $cartItems = $em->getCartItemsWithTotalPrice($user);

    $total = 0.0;

    foreach ($cartItems as $item) {

        $total += $item['totalPrice'];
    }
    return $this->render('cart/panier.html.twig', [
        'cartItems' => $cartItems, 'total'=>$total,
    ]);
}
#[Route ('/suprimer/{id}',name:'app_supprimer')]
public function supprimer($id,PanierProductRepository $em,EntityManagerInterface $manager){
$productpanierId=$em->find($id);
$produit=$productpanierId->getProduct();
$panier=$productpanierId->getPanier();
$produit->removePanier($panier);
$manager->remove($productpanierId);
$manager->flush();
return $this->redirectToRoute('app_panier');
}
    #[Route('/vider', name: 'app_vider')]
    public function vider(PanierProductRepository $em, EntityManagerInterface $manager)
    {
        $user = $this->getUser();
        $panier = $user->getPanier();
        $productPanier = $panier->getProductPanier();

        foreach ($productPanier as $paniers){
           $produit= $paniers->getProduct();
            $panier=$paniers->getPanier();
            $produit->removePanier($panier);
            $manager->remove($paniers);
        }

        $manager->flush();

        return $this->redirectToRoute('app_panier');
    }

    }
