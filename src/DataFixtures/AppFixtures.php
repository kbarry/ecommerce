<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Product;
use App\Repository\ProductRepository;
class AppFixtures extends Fixture
{
    private $categoriesRepository;

    public function __construct(CategorieRepository $categoriesRepository)
    {

    }
    public function load(ObjectManager $manager): void
    {
        $categorieName=['BAGUE',"BOUCLES-D'OREILLES","BRACELETS-FEMME","BRACELETS-HOMME","CEINTURE-HOMME","CHAUSSURES-FEMME","CHAUSSURES-HOMME","COLIER","LUNETTE-FEMME","LUNETTE-HOMME","MONTRE-FEMME","MONTRE-HOMME","SAC-FEMME","vetements-homme"];

      $categories=[];
        for ($i = 0; $i <14; $i++) {
            $categorie=new Categorie();
            $categorie->setName($categorieName[$i]);
            $manager->persist($categorie);
            $categories[$i]=$categorie;
        }
        for ($i=1;$i<=40;$i++) {
            $product = new Product();

            $product->setName($categorieName[0]);
            $product->setDescription('desciption' . $i);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/BAGUE/$i.jpg");
            $product->setCategories($categories[0]);
             $manager->persist($product);
        }
        for($j=1;$j<=20;$j++){

            $product = new Product();

            $product->setName($categorieName[1]);
            $product->setDescription('desciption' . $j);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/BOUCLES-D'OREILLES/$j.jpg");
            $product->setCategories($categories[1]);
            $manager->persist($product);
        }
        for($k=1;$k<=40;$k++){
            $product = new Product();

            $product->setName($categorieName[2]);
            $product->setDescription('desciption' . $k);
            $product->setPrix(mt_rand(10, 100));
            $product->setImage("/images/BRACELETS-FEMME/$k.jpg");
            $product->setCategories($categories[2]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[3]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/BRACELETS-HOMME/$l.jpg");
            $product->setCategories($categories[3]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[4]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 100));
            $product->setImage("/images/CEINTURE-HOMME/$l.jpg");
            $product->setCategories($categories[4]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[5]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/CHAUSSURES-FEMME/$l.jpg");
            $product->setCategories($categories[5]);
            $manager->persist($product);
        }
        for($l=1;$l<=25;$l++){
            $product = new Product();
            $product->setName($categorieName[6]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/CHAUSSURES-HOMME/$l.jpg");
            $product->setCategories($categories[6]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();
            $product->setName($categorieName[7]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/COLIER/$l.jpg");
            $product->setCategories($categories[7]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[8]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/LUNETTE-FEMME/$l.jpg");
            $product->setCategories($categories[8]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[9]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 1000));
            $product->setImage("/images/LUNETTE-HOMME/$l.jpg");
            $product->setCategories($categories[9]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[10]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 100));
            $product->setImage("/images/MONTRE-FEMME/$l.jpg");
            $product->setCategories($categories[10]);
            $manager->persist($product);
        }
        for($l=1;$l<=25;$l++){
            $product = new Product();

            $product->setName($categorieName[11]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 100));
            $product->setImage("/images/MONTRE-HOMME/$l.jpg");
            $product->setCategories($categories[11]);
            $manager->persist($product);
        }
        for($l=1;$l<=30;$l++){
            $product = new Product();

            $product->setName($categorieName[12]);
            $product->setDescription('desciption' . $l);
            $product->setPrix(mt_rand(10, 900));
            $product->setImage("/images/SAC-FEMME/$l.jpg");
            $product->setCategories($categories[12]);
            $manager->persist($product);
        }
        for($m=1;$m<=14;$m++){
            $product = new Product();

            $product->setName($categorieName[13]);
            $product->setDescription('desciption' . $m);
            $product->setPrix(mt_rand(10, 900));
            $product->setImage("/images/vetements-homme/$m.jpg");
            $product->setCategories($categories[13]);
            $manager->persist($product);
        }
        $manager->flush();
    }
}
