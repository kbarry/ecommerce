<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240426224751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_commandes (id INT AUTO_INCREMENT NOT NULL, commandes_id INT DEFAULT NULL, product_id INT DEFAULT NULL, INDEX IDX_5B749EA18BF5C2E6 (commandes_id), INDEX IDX_5B749EA14584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_commandes ADD CONSTRAINT FK_5B749EA18BF5C2E6 FOREIGN KEY (commandes_id) REFERENCES commandes (id)');
        $this->addSql('ALTER TABLE product_commandes ADD CONSTRAINT FK_5B749EA14584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_commandes DROP FOREIGN KEY FK_5B749EA18BF5C2E6');
        $this->addSql('ALTER TABLE product_commandes DROP FOREIGN KEY FK_5B749EA14584665A');
        $this->addSql('DROP TABLE product_commandes');
    }
}
